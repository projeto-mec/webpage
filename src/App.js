import React, { useState, useEffect } from 'react';
import getVantaOptions from './utils/BackgroundColorHandler';
import TextArea from './components/TextArea';
import PanIcon from './components/PanIcon';
import FirebaseInterface from './services/FirebaseInterface';
import './style.css';

export default function App() {
	const [ numPeopleState, setNumPeopleState ] = useState([ null ]);
	const [ ignoreFetchState, setIgnoreState ] = useState(false);
	const firebaseInterface = new FirebaseInterface();
	const changeBackgroundColor = (num_people) => {
		window.BACKGROUND.options = getVantaOptions(num_people);
		console.log(window.BACKGROUND);
		console.log(window.BACKGROUND.update);
	};

	useEffect(() => {
		firebaseInterface.unsubscribe();
		firebaseInterface.changesObserver(null, 'kitchen_data', (docData) => {
			if (docData) {
				if (!ignoreFetchState) {
					setIgnoreState(true);

					const currentDate = docData[docData.length - 1].data();
					const currentDateKeys = Object.keys(currentDate);
					const currentKitchenData = currentDate[currentDateKeys[currentDateKeys.length - 1]];
					
					setNumPeopleState(currentKitchenData.num_people);
					changeBackgroundColor(currentKitchenData.num_people);
				}
			}
		});
	});

	return (
		<div className="App">
			<PanIcon num_people_in_the_kitchen={numPeopleState} />
			<TextArea num_people_in_the_kitchen={numPeopleState} />
		</div>
	);
}
